import os
from concurrent.futures import ProcessPoolExecutor
from itertools import chain, repeat
from functools import partial
import numpy as np
import pandas as pd
import scipy as sc
from scipy import sparse as sp
from tqdm import tqdm

DEFAULT_DATA_FILE_NAME = "Data.xlsx"
SHEET_NAME_FOR_SIMULATED_DATA = "ModelData_r20"
SHEET_NAME_FOR_FIELD_DATA = "PlotData_r15c_rf"

CPU_COUNT = os.cpu_count()

# If on a server with many cores, use only half of them
if CPU_COUNT > 20:
    CPU_COUNT //= 2

if os.name == "nt":
    # Windows can only handle 61 subprocesses
    CPU_COUNT = min(CPU_COUNT, 61)


def get_power_law_coefficients(x, y, log_normal_error):
    """Returns the coefficients of y = a * x**b based on a (log-)linear regression
    between x and y

    Parameters
    ----------
    x : float[]
        Independent variable

    y : float[]
        Dependent variable

    log_normal_error : bool
        Whether to assume a log-normal error. If True, the clustering of
        conspecifics must exceed the clustering of heterospecifics.

    Returns
    -------
    (float, float)
        (a, b). Factor and exponent of the power law
    """
    if log_normal_error:
        x = np.log(x)
        y = np.log(y)
        result = sc.stats.linregress(x, y)
        return np.exp(result.intercept), result.slope

    else:

        def fun(params):
            a, b = params
            return a * np.power(x, b) - y

        result = sc.optimize.least_squares(fun, [y.mean(), -0.01])
        return result.x


def get_transition_rates(
    data,
    max_abundance=30000,
    immigration_rate_per_birth=0,
    consider_individual_species=False,
    consider_niche_differences=False,
    log_normal_error=True,
):
    """
    Calculates the birth and death rates for a given set of data.

    Parameters
    ----------
    data : pandas.DataFrame
        The input data containing the necessary variables.
    max_abundance : int
        The maximum abundance for which to compute the rates.
    immigration_rate_per_birth : float
        The immigration rate per birth.
    consider_individual_species : bool
        Whether to consider individual species or take the mean for all species.
    consider_niche_differences : bool
        Whether to assume niche differences between species.
    log_normal_error : bool
        Whether to consider log-normal error in the calculations.

    Returns
    -------
    increase_rate : numpy.ndarray
        The birth rates for each abundance.
    decrease_rate : numpy.ndarray
        The death rates for each abundance.
    """

    amin, bmin = get_power_law_coefficients(
        data["P_NrInd"], data["P_kff"] - data["P_kfh"], log_normal_error
    )

    # per capita reproduction rate
    rf = data["P_rep"].ravel()[0]

    # per capita background mortality rate (density independent)
    sf = data["P_surv"].ravel()[0]

    # constant, scales plot area to neighborhood area
    cc = 2e-4 * np.pi * data["P_ZOI"].ravel()[0] / data["plotsize/ha"].ravel()[0]

    Hf = data["P_kfh"].ravel()
    Cf = data["P_kff"].ravel()
    if consider_niche_differences:
        Bf = data["P_Bf"].ravel()
    else:
        Bf = 1

    presentAbundance = data["P_NrInd"].ravel()

    # The abundances for which we wanto to compute the birth and death rates
    abundances = np.arange(max_abundance)[:, None]

    if not consider_individual_species:
        # Take mean for individuals in the plot weighted by abundance
        totalPresentAbundance = presentAbundance.sum()
        presentAbundance = totalPresentAbundance / presentAbundance.size
        Hf = (Hf * data["P_NrInd"]).sum() / totalPresentAbundance
        Cf = (Cf * data["P_NrInd"]).sum() / totalPresentAbundance
        if consider_niche_differences:
            Bf = (Bf * data["P_NrInd"]).sum() / totalPresentAbundance

    # corrects amin to get PL(Nf)=m_Cf - m_Hf
    a_sp = np.log(Cf - Hf) - bmin * np.log(presentAbundance)

    # total community size
    JJ = data["P_NrInd[0]"].ravel()[0]

    # mean abundance of species > 50 ind
    Nf = np.round(presentAbundance)

    # uses fitted parameters
    aggregationOfMeanAbundance = np.exp(a_sp) * np.power(Nf, bmin)

    # estimation of parameter betaff based on fitted power law and equilibrium equation
    betaff_sp = -np.log((1 - rf) / sf) / (
        cc * (aggregationOfMeanAbundance * Nf + Hf * (1 - Bf) * Nf + Bf * Hf * JJ)
    )

    aggregation = np.exp(a_sp) * np.power(abundances, bmin)

    if consider_individual_species:
        rf = np.full_like(a_sp, rf)

    increase_rate = (abundances + immigration_rate_per_birth) * rf
    decrease_rate = abundances * (
        1
        - sf
        * np.exp(
            -cc
            * betaff_sp
            * (aggregation * abundances + Hf * (1 - Bf) * abundances + Bf * Hf * JJ)
        )
    )
    decrease_rate[0] = 0

    # Transposition is required if individual_species==True,
    # otherwise it has no effect
    return increase_rate.T, decrease_rate.T


def get_initial_distribution(data, start_abundance=None):
    """
    Calculate the initial condition for the birth-death model.

    Parameters
    ----------
    data : pandas.DataFrame
        A data frame containing the data for the birth-death model.
    start_abundance : int, optional
        The starting abundance for the initial condition.
        If None, the abundance given in the data will be used.

    Returns
    -------
    initialCondition : numpy.ndarray
        The distribution of initial population sizes.
    """
    if start_abundance is None:
        abundanceData = data["P_N0"]
        max_abundance = abundanceData.max() + 1
    else:
        max_abundance = start_abundance + 1

    initial_distribution = np.zeros(max_abundance)

    if start_abundance is None:
        np.add.at(initial_distribution, abundanceData, 1)
        initial_distribution /= initial_distribution.sum()
    else:
        initial_distribution[start_abundance] = 1

    return initial_distribution


def compute_extinction_probabilities(
    data,
    time_points=200,
    start_abundance=None,
    tolerance=1e-6,
    **kwargs,
):
    """Compute the extinction probabilities for given data.

    Parameters
    ----------
    data : pd.DataFrame or list of pd.DataFrame
        The data containing the population dynamics.
    time_points : int, optional
        The number of time points to simulate, by default 200.
    start_abundance : int or None, optional
        The starting abundance for the population, by default None.
        If None, the abundance given in the data will be used.
    tolerance : float, optional
        The tolerance value for the simulation, by default 1e-6.
        The tolerance controls the relative error per time step of
        the simulation.
    **kwargs : keyword arguments
        Additional arguments to be passed to the `get_transition_rates` function.

    Returns
    -------
    list of list of list
        The extinction probabilities for each plot and species in the data.

    """
    if type(data) is pd.DataFrame:
        data = [data]

    initial_distributions = []
    increase_rates = []
    decrease_rates = []
    plot_indices = []

    length = 0
    for i, plot_data in enumerate(data):
        increase_rate, decrease_rate = get_transition_rates(plot_data, **kwargs)
        increase_rates.append(increase_rate)
        decrease_rates.append(decrease_rate)
        initial_distributions.append(
            repeat(
                get_initial_distribution(plot_data, start_abundance), len(increase_rate)
            )
        )
        plot_indices.append(repeat(i, len(increase_rate)))
        length += len(increase_rate)

    extinction_probabilities = [[] for _ in range(len(plot_data))]

    with ProcessPoolExecutor(CPU_COUNT) as pool:
        distributions = pool.map(
            partial(simulate_process, time_points=time_points, tolerance=tolerance),
            chain.from_iterable(initial_distributions),
            chain.from_iterable(increase_rates),
            chain.from_iterable(decrease_rates),
        )
        for plot_index, dist_at_time in zip(
            chain.from_iterable(plot_indices), tqdm(distributions, total=length)
        ):
            extinction_probabilities[plot_index].append([d[0] for d in dist_at_time])

    return extinction_probabilities


def add_extinction_probabilities(data, time_points, start_abundance, **kwargs):
    """Add extinction probabilities to the given data.

    Parameters
    ----------
    data : list of pandas.DataFrame
        The data to which extinction probabilities will be added.
        Each data frame corresponds to a forest plot.
        The data frames will be changed in place.
    time_points : list or float
        The time points at which extinction probabilities will be computed.
    start_abundance : int
        The starting abundance value for the computation of extinction probabilities.
        If None, the abundance given in the data will be used.
    **kwargs : dict
        Additional keyword arguments to be passed to `compute_extinction_probabilities`.

    Returns
    -------
    pandas.DataFrame
        Concatenated data set for all plots with extinction probabilities added.
    """
    if not hasattr(time_points, "__iter__"):
        time_points = [time_points]

    names = ["P_(N0={})[{}]".format(start_abundance, t) for t in time_points]
    extinction_probabilities = compute_extinction_probabilities(
        data, time_points, start_abundance=start_abundance, **kwargs
    )
    for plot_data, prob in zip(data, extinction_probabilities):
        if len(prob) == 1:
            prob = prob[0]
        plot_data[names] = prob

    result = pd.concat(data)
    result.sort_index(inplace=True)
    return result


def add_kwargs_if_not_existent(kwargs_dict, **kwargs):
    """Overwrite the entries of a given dictionary of default keyword arguments
    with the values of the provided keyword arguments.

    Parameters
    ----------
    kwargs_dict : dict
        The dictionary of default keyword arguments.
    **kwargs : dict
        Key-value pairs overwriting the defaults.

    Returns
    -------
    dict
        A dict with the updated keyword arguments.
    """
    for key, value in kwargs.items():
        if key not in kwargs_dict:
            kwargs_dict[key] = value
    return kwargs_dict


def get_default_kwargs(use_simulated_data, **kwargs):
    """
    Return default keyword arguments depending on whether simulated data are used.

    Parameters
    ----------
    use_simulated_data : bool
        Flag indicating whether simulated data are used or not.

    Returns
    -------
    dict
        Default keyword arguments.

    """
    if use_simulated_data:
        add_kwargs_if_not_existent(
            kwargs,
            data_sheet_name=SHEET_NAME_FOR_SIMULATED_DATA,
            check_aggregation=False,
            log_normal_error=False,
            start_abundance=None,
            consider_individual_species=False,
        )
    else:
        add_kwargs_if_not_existent(
            kwargs,
            data_sheet_name=SHEET_NAME_FOR_FIELD_DATA,
            check_aggregation=True,
            log_normal_error=True,
            start_abundance=50,
            consider_individual_species=True,
        )

    return kwargs


def compute_and_save_extinction_probabilities(
    time_points,
    out_file_name,
    use_simulated_data=False,
    data_file_name=DEFAULT_DATA_FILE_NAME,
    **kwargs,
):
    """Compute and save extinction probabilities.

    This function computes the extinction probabilities for a given set of time points
    and saves the results to an output file.

    Parameters
    ----------
    time_points : list or float
        A list of time points at which to compute the extinction probabilities.
    out_file_name : str
        The name of the output file to save the results to.
    use_simulated_data : bool, optional
        Flag indicating whether to use simulated data instead of real data.
        Defaults to False.
    data_file_name : str, optional
        The name of the data file to read from. Defaults to DEFAULT_DATA_FILE_NAME.
    **kwargs : dict
        Additional keyword arguments to customize the computation.

    Returns
    -------
    None
        This function does not return anything. The results are saved to the output file.

    """
    kwargs = get_default_kwargs(use_simulated_data, **kwargs)

    data = filter_species(
        read_data(kwargs.pop("data_sheet_name"), file_name=data_file_name),
        kwargs.pop("check_aggregation"),
    )

    if not hasattr(start_abundance := kwargs.pop("start_abundance"), "__iter__"):
        start_abundance = [start_abundance]

    for start_abundance_ in start_abundance:
        full_data = add_extinction_probabilities(
            data,
            time_points,
            start_abundance=start_abundance_,
            **kwargs,
        )
    full_data.to_excel(out_file_name, index=False)


def simulate_process(
    initial_distribution,
    increase_rate,
    decrease_rate,
    time_points,
    tolerance=1e-6,
    min_index_buffer=50,
    test_step=1,
):
    """
    Simulates a birth-death process over time.

    Parameters
    ----------
    initial_distribution : array_like
        The initial distribution of the population.
        The array's index corresponds to the population size.
    increase_rate : array_like
        The rates at which the population increases for each population size.
    decrease_rate : array_like
        The rates at which the population decreases for each population size.
    time_points : array_like
        The time points at which to evaluate the population distribution.
    tolerance : float, optional
        The tolerance for the numerical integration of one test_step, by default 1e-6.
    min_index_buffer : int, optional
        Indicates how many unlikely large population sizes should be considered at least,
        by default 50.
    test_step : float, optional
        The time step between two updates of the maximal considered population size, by default 1.


    Returns
    -------
    array_like
        The population distribution at the specified time points.
        If only one time point is given, the result is a 1D array.
        Otherwise, the result is a 2D array with one row for each time point.


    Notes
    -----
    The algorithm does not simulate unlikely large populations.
    The considered population sizes are adjused dynamically based on the population distribution.

    """
    max_supported_population = decrease_rate.size
    Pn = np.zeros(max_supported_population)

    # Time until the maximal considered population size is updated

    if hasattr(initial_distribution, "__iter__"):
        Pn[: initial_distribution.size] = initial_distribution
        max_index = min(
            2 * np.nonzero(initial_distribution)[0][-1], max_supported_population - 2
        )
    else:
        Pn[initial_distribution] = 1
        max_index = min(2 * initial_distribution, max_supported_population - 2)

    stay_rate = -increase_rate - decrease_rate

    time = 0

    unique_result = not hasattr(time_points, "__iter__")
    if unique_result:
        time_points = [time_points]

    result = []
    index_buffer = max_index // 2

    for target_time in time_points:
        while time < target_time:

            # Set up the transition rates
            stay_rate_ = stay_rate[:max_index].copy()
            stay_rate_[-1] = 0
            decrease_rate_ = decrease_rate[:max_index].copy()
            decrease_rate_[-1] = 0

            # The ODE system
            def fun(t, x):
                result = stay_rate_ * x
                result[:-1] += decrease_rate_[1:] * x[1:]
                result[1:] += increase_rate[: max_index - 1] * x[:-1]
                return result

            # The Jacobian of the ODE system
            diagonals = np.vstack(
                (increase_rate[:max_index], stay_rate_, decrease_rate_)
            )
            offsets = np.array([-1, 0, 1])
            transition_rates = sp.dia_array(
                (diagonals, offsets), shape=(max_index, max_index)
            )

            # Solve the system
            solution = sc.integrate.solve_ivp(
                fun,
                (time, min(time + test_step, target_time)),
                Pn[:max_index],
                method="BDF",
                jac=transition_rates,
                atol=tolerance,
            )

            # Update the state of the system
            Pn[:max_index] = solution.y[:, -1]
            time = solution.t[-1]

            # Update the maximal considered population size

            # If the maximal possible population size became too likely,
            # we need to increase how many larger population sizes we consider
            if Pn[max_index - 1] >= tolerance:
                index_buffer *= 2
                new_max_index = max_index + index_buffer
            # Otherwise, we adjust the number of considered population sizes
            # based on which population sizes are unlikely
            else:
                larger_indices = np.nonzero(
                    np.cumsum(Pn[max_index - 1 :: -1]) > tolerance
                )[0]
                if larger_indices.size:
                    new_max_index = max_index + (index_buffer - larger_indices[0])
                    if larger_indices[0] > index_buffer // 2:
                        index_buffer = max(min_index_buffer, int(index_buffer * 0.75))
                        new_max_index = max_index + (index_buffer - larger_indices[0])

            Pn[max_index - 1 :] = 0
            max_index = min(max_supported_population - 2, new_max_index)

        result.append(Pn[:max_index].copy())

    if unique_result:
        return result[0]
    else:
        return result


def read_data(sheet_name="ModelData_r20", file_name=DEFAULT_DATA_FILE_NAME):
    """
    Read data from an Excel file and return a list of dataframes for each unique plot.

    Parameters
    ----------
    sheet_name : str, optional
        The name of the sheet in the Excel file to read the data from.
        Default is "ModelData_r20".
    file_name : str, optional
        The name of the Excel file to read the data from.
        Default is DEFAULT_DATA_FILE_NAME.

    Returns
    -------
    list
        A list of pandas dataframes, where each dataframe contains the data for a unique plot.
    """
    data = pd.read_excel(file_name, sheet_name=sheet_name)
    plot_ids = np.unique(data["Plot"])
    return [data[data["Plot"] == plot] for plot in plot_ids]


def filter_species(data, check_aggregation, threshold=50):
    """
    Filter species based on certain criteria.

    Parameters
    ----------
    data : list of dict
        The data containing information about species.
    check_aggregation : bool
        Flag indicating whether to check for the aggregation criterion.
        This is may be necessary for real data, but not for simulated data.
    threshold : int, optional
        The threshold value for filtering species based on population count.
        Defaults to 50.

    Returns
    -------
    list of dict
        The filtered species data based on the specified criteria.
    """
    result = []
    for subdata in data:
        mask = subdata["P_NrInd"] > threshold
        if check_aggregation:
            mask &= subdata["P_kfh"] < subdata["P_kff"]
        result.append(subdata[mask])

    return result


def simulate_scenarios(
    scenario_numbers=None,
    output_time_step=50,
    end_time=2000,
    data_file_name=DEFAULT_DATA_FILE_NAME,
    out_file_name_prefix="Scenario_",
    **kwargs,
):
    """Computes extinction probabilities for different scenarios of interest.

    The results are saved to output files.

    Parameters
    ----------
    scenario_numbers : int or iterable, optional
        The scenario number(s) to simulate. If None, all scenarios will be simulated.
        The scenarios are numbered from 1 to 6.
    output_time_step : int, optional
        The time interval at which extinction probabilities will be computed, in units of time.
    end_time : int, optional
        The end time for the simulation, in units of time.
    data_file_name : str, optional
        The name of the data file to use for the simulation.
    out_file_name_prefix : str, optional
        The prefix for the output file names.
    **kwargs : dict, optional
        Additional keyword arguments to be passed to the `compute_and_save_extinction_probabilities` function.

    Returns
    -------
    None
        This function does not return anything. It saves the results to output files.

    """
    time_points = range(output_time_step, end_time + 1, output_time_step)

    if scenario_numbers is None:
        scenario_numbers = range(1, 7)

    if not hasattr(scenario_numbers, "__iter__"):
        scenario_numbers = [scenario_numbers]

    for scenario_number in scenario_numbers:
        print("Considering scenario", scenario_number)
        out_file_name = out_file_name_prefix + "{}.xlsx".format(scenario_number)
        compute_results = partial(
            compute_and_save_extinction_probabilities,
            time_points=time_points,
            data_file_name=data_file_name,
            out_file_name=out_file_name,
        )
        match scenario_number:
            case 1:
                compute_results(consider_niche_differences=False, **kwargs)
            case 2:
                compute_results(consider_niche_differences=True, **kwargs)
            case 3:
                compute_results(
                    consider_niche_differences=False, immigration_rate_per_birth=0.1, **kwargs
                )
            case 4:
                compute_results(
                    consider_niche_differences=True, immigration_rate_per_birth=0.1, **kwargs
                )
            case 5:
                compute_results(use_simulated_data=True, **kwargs)
            case 6:
                compute_results(
                    consider_niche_differences=False,
                    start_abundance=[12, 18, 25, 36, 50, 71],
                    time_points=200,
                    **kwargs,
                )
        print("Results saved to", out_file_name)
