from birth_death import simulate_scenarios

# Run the simulations of interest
if __name__ == "__main__":
    simulate_scenarios()
