# BirthDeathProcess

This repository contains the code used to estimate extinction probabilities via simulating stochastic birth-death processes using a master equation approach, as described in the paper "Latitudinal scaling of aggregation with abundance and its consequences for coexistence in species rich forests" (currently under review).

To solve the equations, the program uses the implicit integrator [BDF](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.BDF.html) as implemented in the software package [scipy](https://scipy.org/). 

Since the populations could be arbitrarily large in theory, which is infeasible to cover in simulations, a cutoff value needed to be introduced, i.e., a maximal considered population size. This cutoff is adjusted dynamically so that all reasonably likely population sizes are considered.



## Installation

To use the program, you need a [Python 3](https://www.python.org/downloads/) installation. We suggest using a ready-to-go python distribution such as [Anaconda](https://www.anaconda.com/download), which may simplify the installation. This program was tested with Python version 3.11, but all later (and also many earlier versions) should be compatible as well.

To ensure that all required packages are installed, download the file `requirements.txt`. Then, open a command prompt and navigate to the directory, where the file is located. If applicable, load the [virtual python environment](https://docs.python.org/3/tutorial/venv.html) you want to use and execute the following command.

```
pip install -r requirements.txt
```

Note that this installs exactly the package versions that were used for the simulation. Hence, the process may downgrade existing packages if you have newer versions installed. To ensure that no packages are downgraded (at the cost of using a potentially untested setup), use the file `min_requirements.txt` instead.


## Usage

To use the program, you need the python module `birth_death.py` and a data input file (here: `Data.xlsx`). By default, the program computes all the output presented in the paper based on the data used in the paper. 

The program can be run from the console (in the directory in which both input data and the script are located) via a start scipt (e.g. `run_simulations.py`) or directly from the python prompt. The former option simply summarizes the steps of the latter in a script file.

If you want to use the script file, make sure you have all files in the same directory, open a console in the respective folder and execute

```
python run_simulations.py
```

Alternatively, you can start the simulation manually from the python prompt. In the console window, start python by executing

```
python
```

Now, enter the following commands:

```python
from birth_death import simulate_scenarios
simulate_scenarios()
```

The program will create a new Excel file for each considered scenario, containing the input data as well as additional columns with the extinction probabilities at different time points (can be adjusted; see below). With the default settings, the program may run for several hours on a usual computer. If you simply want to test the program, consider adjusting the simulation end time to a small value (e.g. 50). 

To apply the prorgam to a different data set or change for how long the simulation should be run, you can adjust the run by providing keyword arguments:

```python
from birth_death import simulate_scenarios
simulate_scenarios(
    scenario_numbers=[1, 2, 3, 4, 5, 6],
    output_time_step=50,
    end_time=2000,
    data_file_name="Data.xlsx",
)
```

Here, 
 - `scenario_numbers` speciefies which of the six scenarios you want to consider.
 - `output_time_step` speciefies the time interval after which new extiction probabilities should be computed.
 - `end_time` specifies the maximal time for which extinciton probabilities should be computed.
 - `data_file_name` specifies the name of the data input file.

The following scenarios are covered:

1. Use field data; do not consider niche differences; no immigration.
2. Use field data; consider niche differences; no immigration.
3. Use field data; do not consider niche differences; consider immigration.
4. Use field data; consider niche differences; consider immigration.
5. Use simulated data; do not consider niche differences; no immigration.
6. Use field data; do not consider niche differences; no immigration; consider different initial abundances.


## Input data

By default, data input file is expected to contain two sheets, one for simulated data (here, "ModelData_r20"), and one for field data (here "PlotData_r15c_rf"). The names can be chosen differently, but in this case, they must be provided manually for each scenario via the keyword `data_sheet_name="my_data_sheet_name` - or the default values used in the script must be adjusted accordingly (change the global variables `SHEET_NAME_FOR_SIMULATED_DATA` and `SHEET_NAME_FOR_FIELD_DATA`).

The data file must contain certain columns, which are specified below:

| Column      | Description                                                                                                                                                                                                                                                                                                                          |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| P_parset    | Name of the data set for the given species, composed of a plot acronym and a species acronym. For BCI, Sinharaja and MoSingTo we added the census (e.g., C4).                                                                                                                                                                        |
| Plot        | The plot acronym.                                                                                                                                                                                                                                                                                                                    |
| plotsize/ha | The area of the plot/ha.                                                                                                                                                                                                                                                                                                             |
| NrSp        | Number of focal species of the plot (a species is focal species if it has more than 50 individuals with $`\text{dbh} \geq 10\text{cm}`$)                                                                                                                                                                                             |
| P_ZOI       | Radius $`r`$ of the neighborhood; we used in all cases $`r = 15\text{m}`$.                                                                                                                                                                                                                                                           |
| P_rep       | The mean per capita reproduction rate $`r_f`$ within the 5 year census interval, used for all focal species of the plot.                                                                                                                                                                                                             |
| P_surv      | The background per capita survival rate of species $`f`$ in absence of neighbourhood competition within the 5 year census interval.                                                                                                                                                                                                  |
| focalsp     | An indicator number given to each species.                                                                                                                                                                                                                                                                                           |
| P_NrInd     | Observed abundance of the given species.                                                                                                                                                                                                                                                                                             |
| P_NrInd[0]  | Observed total number of individuals with $`\text{dbh} \geq 10\text{cm}`$ in the plot (= community size $`J`$).                                                                                                                                                                                                                      |
| P_kfh       | The index of segregation (or attraction) $`k_{fh}`$ of heterospecifics to the focal species $`f`$.                                                                                                                                                                                                                                   |
| P_kff       | The index $`k_{ff}`$ of conspecific aggregation of the focal species $`f`$.                                                                                                                                                                                                                                                          |
| P_Bf        | The average (relative) heterospecific neighborhood competition strength $`B_{f}`$ suffered by the typical individual of species $`f`$ from one heterospecific individual within its neighborhood with radius $`r`$. If con- and heterospecifics compete equally, $`B_{f} = 1`$. In case of niche differences, we have $`B_{f} < 1`$. |
| P_N0        | Initial abundance of species $`f`$ in the individual-based simulation. Only for analysis of output of individual-based simulations.                                                                                                                                                                                                  |
| P_betaIBM   | Conspecific individual-level interaction coefficient $`\beta_{ff}`$ of the focal species $`f`$. Only for analysis of output of individual-based simulations.                                                                                                                                                                         |


## Support

If you need support with this program, file an [issue on Gitlab](https://git.ufz.de/oesa/birthdeathprocess/-/issues) or contact the corresponding authors of the paper.


## Contributing

If you find an error in the code, you are welcome to correct it directly and file a merge request on GitLab. Please document and explain the changes so that the developers can easily understand what you are fixing and how.


## Authors and acknowledgment

This software was written by Samuel M. Fischer and Thorsten Wiegand, Department for Ecological Modelleing, Helmholtz Centre for environmental research - UFZ, Leipzig, Germany. If you want to use it for your own research, please cite the paper. 


## License

This software is licensed under the GNU General Public License Version 3; see LICENSE.txt.

If you want to use the software for your own research, please cite the paper. 
